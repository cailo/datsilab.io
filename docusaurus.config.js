const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

// With JSDoc @type annotations, IDEs can provide config autocompletion
/** @type {import('@docusaurus/types').DocusaurusConfig} */
(module.exports = {
    title: 'Documentación - SHAeI',
    tagline: 'Documentación de la Secretaría de Hacienda, Administración e Infraestructura',
    url: 'https://cailo.gitlab.io/docbook',
    baseUrl: '/datsi.gitlab.io/',
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',
    favicon: 'img/favicon.ico',
    organizationName: 'datsi-dev', // Usually your GitHub org/user name.
    projectName: 'docbook', // Usually your repo name.

    presets: [
        [
            '@docusaurus/preset-classic',
            /** @type {import('@docusaurus/preset-classic').Options} */
            {
                docs: {
                    sidebarPath: require.resolve('./sidebars.js'),
                    // Please change this to your repo.
                    // editUrl: 'https://gitlab.com/datsi-dev/docbook/edit/main/website/',
                },
                //blog: {
                //    showReadingTime: true,
                //    // Please change this to your repo.
                //    editUrl: 'https://gitlab.com/datsi-dev/docbook/edit/main/website/blog/',
                //},
                theme: {
                    customCss: require.resolve('./src/css/custom.css'),
                },
            },
        ],
    ],
    plugins: [
        [
            '@docusaurus/plugin-content-docs',
            {
                id: 'technical',
                path: 'technical',
                routeBasePath: 'technical',
                sidebarPath: require.resolve('./sidebarsTechnical.js'),
                // ... other options
            },
        ],
    ],

    themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
        ({
        navbar: {
            title: 'SHAeI',
            logo: {
                alt: 'UNSL',
                src: 'img/unsl.png',
            },
            items: [{
                    type: 'doc',
                    docId: 'intro',
                    position: 'left',
                    label: 'Documentación',
                },
                {
                    type: 'doc',
                    docId: 'intro',
                    position: 'left',
                    label: 'Technical',
                    docsPluginId: 'technical',
                },
                //{ to: '/blog', label: 'Blog', position: 'left' },
                //{
                //    href: 'https://github.com/facebook/docusaurus',
                //    label: 'GitHub',
                //    position: 'right',
                //},
            ],
        },
        footer: {
            style: 'dark',
            // links: [{
            //         title: 'Docs',
            //         items: [{
            //             label: 'Tutorial',
            //             to: '/docs/intro',
            //         }, ],
            //     },
            //     {
            //         title: 'Community',
            //         items: [{
            //                 label: 'Stack Overflow',
            //                 href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            //             },
            //             {
            //                 label: 'Discord',
            //                 href: 'https://discordapp.com/invite/docusaurus',
            //             },
            //             {
            //                 label: 'Twitter',
            //                 href: 'https://twitter.com/docusaurus',
            //             },
            //         ],
            //     },
            //     {
            //         title: 'More',
            //         items: [{
            //                 label: 'Blog',
            //                 to: '/blog',
            //             },
            //             {
            //                 label: 'GitHub',
            //                 href: 'https://github.com/facebook/docusaurus',
            //             },
            //         ],
            //     },
            // ],
            copyright: `Copyright © ${new Date().getFullYear()} DATSI - UNSL`,
        },
        prism: {
            theme: lightCodeTheme,
            darkTheme: darkCodeTheme,
        },
    }),
});
