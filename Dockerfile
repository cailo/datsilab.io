FROM node:lts

WORKDIR /app/website

EXPOSE 3000 35729
COPY ./docs /app/docs
COPY ./technical /app/technical
COPY ./website /app/website
RUN yarn install

CMD ["yarn", "start"]