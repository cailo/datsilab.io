# Control
>REFERENCIAS:

>[https://www.howtoforge.com/using-old-debian-versions-in-your-sources.list](https://www.howtoforge.com/using-old-debian-versions-in-your-sources.list)
>[https://wiki.centos.org/HowTos/Network/SecuringSSH](https://wiki.centos.org/HowTos/Network/SecuringSSH)
>[http://deartpix.blogspot.com/2012/12/ssh.html](http://deartpix.blogspot.com/2012/12/ssh.html)


## Configurar SSH “/etc/sshd_config”
Cambiar a Protocolo
```
Protocol 2
```
Cambiar Puerto
```
Port 9122 
```
Deshabilitar autenticación por contaseña, solo con certificado
```
PasswordAuthentication no
UsePAM no
```
```
systemctl restart sshd
```
## Configurar SUDO y agregar los usuarios al grupo:
```
gpasswd -a glopez sudo
gpasswd -a cailo sudo
gpasswd -a siu sudo #se usa solo en la integracion
```
## Controlar /etc/hosts
```
nano /etc/hosts
```
## Firewall Persistente
Revisar /etc/rc.local y eliminar/comentar /etc/firewall.sh
>Antes usabamos un script para cargar el firewall.

```
apt install iptables-persistent netfilter-persistent
```
``` 
netfilter-persistent save
netfilter-persistent start
```
**Guardar las reglas**
```
iptables-save  > /etc/iptables/rules.v4   
iptables-restore  < /etc/iptables/rules.v4
```
**Restaurar las reglas**
```
ip6tables-save > /etc/iptables/rules.v6
ip6tables-restore < /etc/iptables/rules.v6
```
```
systemctl {stop/start/restart} netfilter-persistent
```

## Instalar y ejecutar ROOTKIT HUNTER
```
apt install rkhunter
```