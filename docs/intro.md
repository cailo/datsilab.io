---
sidebar_position: 1
---

# Introducción 

Esta es una documentación es suministrada por el DATSI para la Secretaría de Hacienda, Administración e Infraestructura con el fin de suministrar un punto de acceso a la información que sea de ayuda para los usuarios funcionales/técnicos, la idea de la misma es que vaya creciendo con el aporte de los usuarios de los distintos sistemas.

## Comenzando

Usted puede navegar por las secciones del menu, con el fin de agilizar su busqueda.

## Colaboración

Pedimos la colaboracón de los usuarios de los sistemas, para agregar más información o corregirla.
Usted puede enviar un mail a **datsi@email.unsl.edu.ar**.



