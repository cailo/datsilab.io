# Usuarios

## Sumario

1. [Gestión de Usuarios](#usuarios)
2. [Guia de Actualización](#actualizacion)

## Gestión de Usuarios<a name="usuarios"></a>

**Acceder a toba_usuarios:**

[https://pilaga.unsl.edu.ar/toba_usuarios](https://pilaga.unsl.edu.ar/toba_usuarios)

>Nota: Se recomienda acceder con Firefox, por problemas de visualización.

Ir a la pestaña “Usuarios” , “Mantenimiento de usuarios”
>**Comprobar si el usuario existe, sino existe  cargarlo.**

![image](../../../static/img/pilaga/toba_usuario.png)

En la pestaña “Perfil” (Únicamente en el proyecto Pilaga) | Seleccionar el perfil que corresponda.

>Todas las facultades son tratadas como “Dependencias”
Solo los usuario de Dependencias deben ser cargados en Toba y Pilaga

:::danger
NUNCA ASIGNAR UN USUARIO A “PERFIL TOBA”
:::

### Usuarios de dependencias:

1. Acceder a al sistema: [https://pilaga.unsl.edu.ar/](https://pilaga.unsl.edu.ar/)

2. Ir a la opción “Administración” > “Configurar Unidad de Gestión”
Selecionar Facultad -> Pestaña:  “Usuarios”