# Actualización

**Documentación Oficial SIU-Pilaga**  
[documentacion.siu.edu.ar/wiki/SIU-Pilaga](https://documentacion.siu.edu.ar/wiki/SIU-Pilaga)

## Guia de Actualización<a name="actualizacion"></a>

**Listado de Servidores Pilaga:**
1. Producción
2. Huarpes (Testing)
3. Histórico
4. Pilagabeta (Ecosistema)

### Backup
Backup archivos de configuración:
```   
tar -czvf  SIU-Pilaga_VIEJO-PreUpdateNUEVO.tar.gz  /siu/web/SIU-Pilaga_Viejo/
```
Ejemplo:
```
tar -czvf  SIU-Pilaga_3.8.4-PreUpdate385.tar.gz  /siu/web/SIU-Pilaga_3.8.4/
```
Backup Base de Datos (como root):
```
cd /siu/backups/
./backup_pilagadb.sh
```
### Producción: 

:::tip
Cuando se realiza una actualización de sistema de producción se debe realizar backups de la base de datos de ARAI y SIU-Diaguita
:::

```
cd /siu/backups/
./backup_arai.sh y ./backup_diaguita.sh 
```

### Rollback
Directorio de configuración (Solo Pilaga):
Eliminar los directorios de configuración “SIU-Pilaga_Nueva” y “SIU-Pilaga_Vieja”; a continuación  descomprimir el archivo “SIU-Pilaga_VIEJO-PreUpdateNUEVO.tar.gz”
```
tar -xzvf  SIU-Pilaga_VIEJO-PreUpdateNUEVO.tar.gz
```
**Bases de Datos (SIU-Pilaga, SIU-Diaguita y SIU-Arai):**
>NOTA: Únicamente se debe hacer roolback de las bases de Arai y Diaguita, cuando se ha realizado una actualización del sistema de producción. Hacer uso del script levanta_backup.sh para levantar las bases de datos, el backup debe estar en el mismo directorio que el script.

Ejemplo:
```
cd /siu/backups/
./levanta_backup.sh <backup_DB> <Nombre_DB>
```