# FAQ - Impresora de cheques

1. **LA PC ESTÁ APAGADA**

Llamar a Victor, Interno (5147).
Hacer ping a la dirección IP 10.230.3.46

2. **SE COLGÓ / CORTÓ LA RED**

La mejor solución es reiniciar la PC y levantar la chequera nuevamente.

3. **SE CORTÓ LA LUZ**

Prender UPS,  Prender PC,  Levantar chequera  *(Paso 2)


## Iniciar servidor de cheques

1. Ingresar al servidor de cheques a través de SSH:
```
ssh -p 9122 nico@10.230.3.46 contraseña: (la de la yerba)
```
2. Cambiar a usuario root:
```
su -  contraseña: (la del termo)
```
3. Controlar si está montado SIU-Pilaga en el servidor de cheques:
```
mount
```
>Si está montado, se debe observar la siguiente línea:
```
nico@190.122.226.54:/siu/cheques on /home/nico/cheques type 
fuse.sshfs
```  
>Si está montado, controlar que se vean los archivos de
190.122.226.54:/siu/cheques/ en /home/nico/cheques/ 

4. Si no esta montada, montar:
```
./montar_cheques.sh  
```
5. Verificar que la si el proceso de la CHEQUERA está corriendo:
```
ps -a | grep AUTOCHEQ
```
>Si está corriendo, detener el proceso.

6. Iniciar

:::tip
Usar terminal pequeña (25 Líneas) al iniciar AUTOCHEQ.EXE 
:::

```
cd /home/nico/srvcheq/
screen dosemu AUTOCHEQ.EXE
```
## Otros Problemas

**SE COLGÓ LA IMPRESORA**

Apagar, desenchufar la impresora y esperar a que se apague por completo.
Enchufar y prender la impresora.