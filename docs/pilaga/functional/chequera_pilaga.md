# Cambio de Talonario
>Chequera

**Acceder al sistema**

[https://pilaga.unsl.edu.ar](https://pilaga.unsl.edu.ar)

1. En el menú de Pilagá ir a: “Maestros > Elementos tesorería > ABM - Chequeras”
2. Controlar chequera cargada anterior (Configuración chequera anterior)
3. Configurar nueva chequera: (Copiar datos carga anterior)

:::tip Solo modificar N° de Chequera , N° de Inicio y N° de Finalización
:::

## Activar Chequera
1. En el menú de SIU Pilagá ir a:
2. Ejecutar la opción filtrar con los campos vacíos y luego seleccionar el banco en cuestión: (Para acceder hacer click en la flecha)
3. Seleccionar Chequera a activar:

## Consideración Villa Mercedes
Cuenta Bancaria: “Cuenta Recursos Propios VM”

