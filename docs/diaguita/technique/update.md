# Actualización

**Documentación Oficial SIU-Diaguita**
[documentacion.siu.edu.ar/wiki/SIU-Diaguita](https://documentacion.siu.edu.ar/wiki/SIU-Diaguita)

:::tip My tip
Use this awesome feature option
:::

:::danger Take care
This action is dangerous
:::

## Sumario

1. [Copia de Seguridad](#Backup)
2. [Pre-Actualización](#PreUpdate)
3. [Actualización](#Update)
4. [Post-Actualización](#PostUpdate)
5. [Desactivar Mantenimiento](#Up)

## Servidores

- Diaguita     (Producción)
- Ona          (Pruebas)
- Diaguitabeta (Ecosistema)

**Rutas de los instaladores**

Ruta de instalación Producción: "/siu/"

## Copia de Seguridad<a name="Backup"></a>

>NOTA: Antes de realizar cualquier proceso de actualización, se debe realizar una copia de seguridad, tanto de la Base de Datos como de la Instalación Actual.

**1. Backup SIU-Diaguita_Anterior / Archivos de configuración**
```
tar -czvf SIU-Diaguita_Anterior-PreUpdateNUEVO.tar.gz SIU-Diaguita_Anterior
```

**2. Backup Base de datos**

>NOTA: En  el servidor de producción, hacer Backup de las bases de los Sistemas SIU-Pilaga y SIU-Arai Proveedores

```
cd /siu/backups
./backup_diaguita.sh
```

## Pre-Actualización<a name="PreUpdate"></a>

**1. Descargar la ultima versión del modulo a actualizar desde el siguiente link**
[comunidadsiu.edu.ar](https://portal.comunidad.siu.edu.ar)

**2. Copiar el instalador al servidor y descomprimir instalador**

```
scp SIU-Diaguita_Nuevo usuario@ip-servidor
```
Archivo ".rar" (Usar el parametor x para que respete el arbol de directorios)
```
unrar x SIU-Diaguita_Nuevo.rar
```
Archivo ".zip"
```
unzip SIU-Diaguita_Nuevo.zip
```
Archivo ".tar.gz"
```
tar -xzvf SIU-Diaguita_Nuevo.tar.gz
```

**3. Instalar Composer**
Este proceso debe ejecutarse dentro del **SIU-Diaguita_Nuevo**. Genera como resultado la carpeta vendor dentro de dicho instalador.

>NOTA: Este comando **NO** debe ejecutarse como usuario Root/Sudo.
Antes de ejecutar el comando, asegurarse que en el parametro "allow_url_fopen" del archivo php.ini este "On".

```
cat /etc/php/7.3/cli/php.ini | grep allow_url_fopen
```
```
composer install --no-dev
```

>Ante un error en el proceso de instalación de composer, borrar la carpeta vendor y descargar desde la pagina el vendor correspondiente a la versión que se desea instalar.

**4. Permisos de ejecucion al instalador nuevo**
```
cd SIU-Diaguita_Nuevo
chmod +x bin/instalador
```

**5. Copiar SIU-Diaguita_Nuevo a la ruta donde se alojan los instaladores sistemas**
```
cp -rv SIU-Diaguita_Nuevo /siu/
```

**6. Verificar en el instalador anterior**

>SIU-Diaguita_Anterior/instalacion/entorno_toba.env

Controlar rutas de la instalación
```
cat SIU-Diaguita_Anterior/instalacion/entorno_toba.env
```
Controlar parametro  **usar_perfiles_propios = "1"**
```
cat SIU-Diaguita_Anterior/instalacion/i__produccion/instancia.ini
```

**7. Copiar el archivo de configuración instalador.env** desde la SIU-Diaguita_Anterior a SIU-Diaguita_Nueva.
Ejemplo:
```
cp SIU-Diaguita_3.1.0/instalador.env SIU-Diaguita_3.1.1/
```
>NOTA: Controlar los parametros seteados en el archivo instalador.env

**8. Detener Servicio JasperReport**
```
cd /etc/init.d
./diaguita_reportes.sh stop
```

## Actualización<a name="Update"></a>
>NOTA: Se debe encontrar dentro del directorio de la **SIU-Diaguita_Nueva**

**1. Verificar configuración cargada en el archivo instalador.env**:
```
./bin/instalador proyecto:verificar
```

**2. Ejecutar proceso de Actualización**
>NOTA: La opcion "-n" hace que el proceso no sea interactivo.
```
./bin/instalador proyecto:actualizar --instalacion-anterior /RUTA_INST_ANTERIOR -n
```

## Post-Actualización<a name="PostUpdate"></a>

**1. Modificar paths archivo de configuración Apache2** *"/etc/apache2/sites-available/www-diaguita.conf"*

**Archivo de configuración www-diaguita.conf**

**HTTPS/SSL**
```
<VirtualHost ip_servidor:443>
        ServerName <nombre_dominio.unsl.edu.ar>
        DocumentRoot <PATH_NUEVO_INSTALACION>/www
        ErrorLog /var/log/diaguita_error.log
        LogLevel error

        CustomLog /var/log/diaguita_acces.log combined
        Include <PATH_NUEVO_INSTALACION>/instalacion/toba.conf
        SSLEngine on

        #Archivo anti-boots (Google/Bing/Facebook/Amazon/etc)
        <Location "/robots.txt">
            SetHandler None
            Require all granted
        </Location>
        Alias /robots.txt <PATH_NUEVO_ISNTALACION>/www/robots.txt

        #VENCE: DD/MM/YYYY - <Certificados SSL>
        SSLCertificateFile      /etc/apache2/ssl/2021-unsl.edu.ar.crt
        SSLCertificateKeyFile   /etc/apache2/ssl/2021-unsl.key

        #Habilita HTTP/2, si está disponible
        Protocols h2 http/1.1

</VirtualHost>
```
**2. Cambiar propietario SIU-Diaguita_Nueva**
>Es necesario cambiar del propietario de la nueva instalación a www-data para que apache2 pueda visualizar correctamente el sistema.

```
chown -R www-data:www-data <PATH_INSTALACION_NUEVA>
```

**3. Reiniciar  Servicio Apache2**
Para que el servidor tome la nueva configuración, es necesario recargar/reiniciar el servicio con el siguiente comando:

Recargar:
```
sudo apache2 reload
```
Reiniciar:
```
sudo apache2 restart
```

**7. Verificar configuración VirtualHost de Apache2**
Controla errores de sintaxis en los archivos de configuración
```
sudo apachectl configtest
```
**8. Iniciar Servicio JasperReport**
```
cd /etc/init.d
```
- Editar el script *diaguita_reportes.sh* modificando la ruta a la Nueva Instalación.
Ejemplo:
```
nano diaguita_reportes.sh

# Ruta a la instacion de SIU-Diaguita
# ATENCION: debe definir la ruta a la instalacion SIU-Diaguita
PATH_DIAGUITA="/siu/SIU-Diaguita_Nueva"

```
- Iniciar servicio
```
./diaguita_reportes.sh star
```

**9. Controlar archivo smtp.ini**
```
[notificaciones]
host = "smtp.gmail.com"
puerto = "587"
auth = "1"
usuario = "contrata.unsl@gmail.com"
clave =
seguridad = "tls"
nombre_from = "Dirección de Compras y Contrataciones - UNSL"
helo = "0"
from = "contrata.unsl@gmail.com"
auto_tls = "1"
```
**10. Controlar tareas programadas en crontab**
Modificar la ruta de las tareas programadas a SIU-Diaguita_Nueva
```
crontab -e

#Diaguita
0 5 * * * /siu/SIU-Diaguita-3.1.1/bin/limpiar_temporales.sh
*/20 * * * * /siu/SIU-Diaguita-3.1.1/bin/toba proyecto arai_sincronizacion -pdiaguita -iproduccion > /dev/null
*/5 * * * * /siu/SIU-Diaguita-3.1.1/bin/diaguita_notificaciones.sh

```
## Activar Sistema / Desactivar Modo Mantenimiento<a name="Up"></a>
Al finalizar el proceso el sistema queda configurado en modo mantenimiento, para desactivar el modo mantenimiento ejecutar el siguiente comando:
```
./bin/instalador instalacion:modo-mantenimiento --sin-mantenimiento
```


[Menu Principal](#Home)