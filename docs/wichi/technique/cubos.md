# Documentación Wichi

**Documentación Oficial SIU-Wichi**  
[documentacion.siu.edu.ar/wiki/SIU-Wichi](https://documentacion.siu.edu.ar/wiki/SIU-Wichi)

:::danger
Antes de realizar cualquier proceso de carga de cubos, realizar una backup de las bases de datos.
```
cd /siu/backups
./backup_all.sh
```
:::

## Indice



1. Cubos
- 1.1 [Carga RHUN](#RHUN)
- 1.2 [Carga Mapuche](#CMapuche)
- 1.3 [Carga Araucano](#Araucano)
- 1.4 [Pilaga]
- 1.5 [Diaguita]

2. Backup
3. Actualización

## 1.Cubos
## 1.1 Carga RHUN<a name="RHUN"></a>

1. Subir el cubo rhun al servidor

>NOTA: Debe ser el version 2 y contar con un 34 archivos.

2. Ir al directorio "datos_rhun"

```
cd  /usr/local/pentaho/wichi/datos_rhun
```

2- Eliminar los archivos viejos dentro del directorio "datos_rhun"

3- Copiar los datos nuevos enviados al directorio "datos_rhun"

>NOTA: Controlar que sean que en el directorio hayan 34 archivos 

```
cp -r archivos_nuevos/* /usr/local/pentaho/wichi/datos_rhun/
```

4. Ir al Path donde se encuentran los Scripts
```
cd /usr/local/pentaho/scripts
```

5. Ejecutar script: 

>Ejemplo:./cargar_rhun.sh 201810 0

```
./cargar_rhun.sh <Periodo> <Opción>
Periodo: Formato AñoMes, "YYYYMM". 
Opción: 0 Nuevo archivo | 1 Actualización
```

**Post-Actualización**

1. Ingresar a Wichi URL: [https://wichi.unsl.edu.ar:8443](https://wichi.unsl.edu.ar:8443)

2. Actualizar cache y otros componentes -> "Herramientas"

3. Recargar Cubo
Archivo/Saiku Analytics 

4. Verificar que los datos se hayan cargado correctamente. 

>NOTA: Realizar las consultas con la herramienta Saiku de Pentaho

## 1.2 Carga Mapuche<a name="CMapuche"></a>

## 1.3 Carga Araucano<a name="Araucano"></a>

# Readme Wichi

# **Índice**
1. [Actualización de Transparencia](#actualizacion)
2. [Carga de Personas de Mapuche a Diaguita](#cargapersonas)
3. [Cubo Mapuche - Wici](#cubomapuchewichi)
4. [Carga Cubo Rhun - Wichi](#cuborhunwichi)
5. [Gestión de Usaurios SIU-Wichi](#gestionusuarios)

## 1. Actualización Portal de Transparencia<a name="actualizacion"></a>

>Nota: Para actualizar el Portal de Transparencia, se deben actualizar los 4(cuatro) cubos. SIU-Pilaga, SIU Diaguita, SIU-Araucano y SIU-Mapuche. Este último solo se genera en lapsos específicos, por lo que debe ser solicitado.

Ir al path:
```
cd /usr/local/pentaho/scripts
```

### Actualización CUBOS:

#### SIU-Pilaga:
```
screen ./act_pilaga-2019.sh
```
#### SIU-Diaguita:

```
screen ./act_diaguita.sh
```

#### SIU-Araucano:
Entrar a SIU-Araucano y ejecutar los siguientes pasos.

1. URL: http://araucano.siu.edu.ar/

2. Seleccionar botón → **Administración** → **Exportar Datos para los CUBOS**
<!-- ![image](../../../img/wichi/actualizacionTransparencia/image1.png) -->

3. Seleccionar botón → **Generar**
<!-- ![image](../../../img/wichi/actualizacionTransparencia/image2.png) -->

4. Seleccionar botón → **Descargar**
<!-- ![image](../../../img/wichi/actualizacionTransparencia/image3.png) -->

#### SIU-Mapuche
>Es necesario que pasen la información.

1. Acceder al servidor SIU-WICHI
```
ssh wichi
```

2. Ir al directorio ```/siu/wichi```
```
cd /siu/wichi
```

3. Renombrar el archivo ```O3.zip``` o ```O3(1).zip``` por un nombre identificatorio del mes.
  **Ejemplo:** ```mv O3.zip  O3-Agosto.zip``` (periodo a cargar/actualizar).

4. Vaciar los directorios ```/siu/wichi/mapuche-iso8859 & /siu/wichi/mapuche-utf8```

5. Copiar los archivos ```.txt``` de ```O3-Agosto.zip``` al directorio ```/siu/wichi/mapuche-iso8859```

6. Cambiar encoding ```iso-8859``` a ```UTF8``` ```./convEncodig.sh <origen> <destino> (iso-8859 => utf8)```

 **Ejemplo:**
 ```
 ./convEncoding.sh /siu/wichi/mapuche-iso8859 /siu/wichi/mapuche-utf8
 ```

7. Copiar los archivos de ```mapuche-utf8``` a ```/usr/local/pentaho/wichi/datos_mapuche```

 Ejecutar el script ```./cargar_mapuche.sh 201911 0```
 **Uso:**
```
cargar_mapuche.sh <periodo> <nuevo/actualización>
# 0 = Nuevo / 1 = Actualización
```


>Una vez cargados todos los cubos ejecutar el Script "transparencias.sh"

### Actualizar Cache y Saiku WICHI
Ingresar https://wichi.unsl.edu.ar

#### Cache
Seleccionar botón → Herramientas → Actualizar →
<!-- ![image](../../../img/wichi/actualizacionTransparencia/image4.png) -->
Actualizar todas las opciones

#### SAIKU
Seleccionar botón → Archivo → Nuevo → Saiku Analytics
Seleccionar cualquier Cubo y dar clic en la opción "refrescar cubos"
En el menú principal seleccionar→ SIU-Wichi → Administrador Servidor→ Configuración → "4- Actualización de Datos.wcdf"

<!-- ![image](../../../img/wichi/actualizacionTransparencia/image5.png) -->

## 2. Carga de Personas de Mapuche a Diaguita<a name="cargapersonas"></a>
1. Ingresar al servidor SIU-DIAGUITA:
```
ssh usuario@diaguita
su -  #Pasar a root
```

2. Ir al directorio:
```
cd /siu/migra-persona.mapuche
```

3. Ejecutar el script:
```
./cargar-dh01.sh
```

4. Una vez finalizada la operación anterior ejecutar:
```
php migrar.php
```
>Si ```migrar.php``` devuelve un mensaje de error, seleccionar que "NO" se desea continuar e imprimir el mensaje de error para realizar la corrección que corresponda de manera manual.

>EN CASO DE ERROR CONTROLAR SI TIENE BIENES ASIGNADOS EN SIU-DIAGUITA

### Soluciones
#### SI NO TIENE BIENES ASIGNADOS:
1. Ir al directorio:
```
cd /siu/migra-persona.mapuche
```

2.Editar el archivo y cargar el número de legajo a corregir:
```
vim eliminar-problema-lado-diaguita.sql
```
<!-- ![image](../../../img/wichi/cargaPersonas/image1.png) -->
3.Editar el archivo y cargar el script ".sql" a utilizar:
```
vim eliminar-problema.sh
```
<!-- ![image](../../../img/wichi/cargaPersonas/image2.png) -->
4.Ejecutar:
```
./eliminar-problema.sh
```

## 3. Cubo Mapuche - Wichi<a name="cubomapuchewichi"></a>
### En Servidor Wichi
1. BACKUP
 1. Realizar un  backup de las bases de datos: (hibernate / jackrabbit /quartz / siu_wichi)
 ```
 /siu/backups/backup_all.sh
 ```
 2. Generar archivo TAR:
 ```
 tar -czvf <fecha_backupPreCargaMapucheMMYYYY.tar.gz>  <fecha_backup>
 ```
 
2. ARCHIVO LOG:
```
/usr/local/pentaho/pentaho-server/tomcat/logs/pentaho.log
```

3. VERIFICACIÓN DE ACTUALIZACIÓN:
  1. Entrar a https://wichi.unsl.edu.ar:8443/pentaho/Home
  2. Limpiar cache (Tools -> Refresh)

    - System Settings
    - Reporting Metadata
    - Global Variables
    - Mondrian Schema Cache
    - Reporting Data Cache
    - CDA Cache
  
  3. Refrescar cubos (File -> New Saiku Analytics)
  >Seleccionar un cubo cualquiera y hacer click en el icono de refresh
  >

  4. Ejecutar consulta: (gonzalo -> Control_CargaMapuche.saiku)




## 4. Carga Cubo Rhun - Wichi<a name="cuborhunwichi"></a>
>Documentación: http://documentacion.siu.edu.ar/wiki/SIU-Wichi
>

### REALIZAR BACKUPS DE LAS BASES DE DATOS "WICHI"
```
/siu/backup/backup_all.sh
```

### PRE-UPDATE
1. Ir al directorio "datos_rhun"
```
cd  /usr/local/pentaho/wichi/datos_rhun
```

2. Eliminar los archivos viejos dentro del directorio
```
rm  ./* o con mc
```

3. Copiar los datos nuevos enviados al directorio "datos_rhun"
```
mc o cp -r archivos_nuevos/* /usr/local/pentaho/wichi/datos_rhun/
```
>Controlar que sean 34 (Archivos)

### ACTUALIZACIÓN
1. Ir al Path donde se encuentran los Scripts
```
cd /usr/local/pentaho/scripts
```

2. Ejecutar script: (Ejemplo:./cargar_rhun.sh 201810 0)
```
./cargar_rhun.sh <Periodo> <Opción>
```
>**Periodo:** Formato AñoMes, "YYYYMM".
 **Opción:** 0 Nuevo archivo | 1 Actualización.

### POST-UPDATE
1. Ingresar a Wichi (URL: https://wichi.unsl.edu.ar)

2. Actualizar cache y otros componentes -> "Herramientas"

3. Recargar Cubo -> "Archivo/Saiku Analytics"

4. Verificar que los datos se hayan cargado correctamente.
>(Realizar las consultas con la herramienta Saiku de Pentaho)

## 5. Gestión de Usaurios SIU-Wichi<a name="gestionusuarios"></a>

### Agregar Usuario (incluir en la planilla):

1. Ir a Examinar -> Administración

 <!-- ![image](../../../img/wichi/gestionUsuarios/image1.png) -->
 
2. Hacer click en el símbolo "+" (Agregar) o "x" (Eliminar)

 <!-- ![image](../../../img/wichi/gestionUsuarios/image2.png) -->
 
3. Ingresar usuario y contraseña:

 <!-- ![image](../../../img/wichi/gestionUsuarios/image3.png) -->
 
4. Seleccionar usuario y asignar Roles (Según corresponda)

 <!-- ![image](../../../img/wichi/gestionUsuarios/image4.png) -->